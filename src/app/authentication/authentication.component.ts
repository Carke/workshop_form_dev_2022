import { Component, OnInit } from '@angular/core';

import { FetchService } from '../fetch.service';
import {FormControl, FormGroup, Validator, Validators} from "@angular/forms";

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.sass'],
})
export class AuthenticationComponent implements OnInit {


  formLogin: FormGroup = new FormGroup({
    mail: new FormControl('', [Validators.required]),
    password : new FormControl('', [Validators.required])
  })

  display: boolean = false;

  constructor(private fetchservice:FetchService) {

  }

  ngOnInit() {

  }

  showDialog() {
    this.display = true;
  }
  dismissDialog() {
    this.display = false;
  }
  login() {
    this.fetchservice.tryLogin((<HTMLInputElement>document.getElementById('loginInput')).value, (<HTMLInputElement>document.getElementById('passwordInput')).value).subscribe(data => {
      type Data = {
        response?: string;
      }
      const dataObj: Data = data;
      if (dataObj.response === "ok") {
        window.location.href = (<HTMLFormElement>document.getElementById('loginForm')).action;
      }
    })
  }

}
