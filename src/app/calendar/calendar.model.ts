export class Calendar {
    id!: Number;
    name!: String;
    professor!: String;
    hourStart!: String;
    hourEnd!: String;
    date!: String;
}