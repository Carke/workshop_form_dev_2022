import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarComponent } from './calendar/calendar.component';
import { CalendarHeaderComponent } from './calendar-header/calendar-header.component';
import { CalendarMonthComponent } from './calendar-month/calendar-month.component';
import { CalendarWeekComponent } from './calendar-week/calendar-week.component';
import { CalendarListComponent } from './calendar-list/calendar-list.component';
import { CalendarListItemComponent } from './calendar-list-item/calendar-list-item.component';
import { SignaturesComponent } from './signatures/signatures.component';

import { PanelModule } from 'primeng/panel';
import { TabMenuModule } from 'primeng/tabmenu';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';


@NgModule({
  declarations: [
    CalendarComponent,
    CalendarHeaderComponent,
    CalendarMonthComponent,
    CalendarWeekComponent,
    CalendarListComponent,
    CalendarListItemComponent,
    SignaturesComponent,
  ],
  imports: [
    CommonModule,
    PanelModule,
    TabMenuModule,
    DialogModule,
    TableModule,
    ButtonModule
  ]
})
export class CalendarModule { }
