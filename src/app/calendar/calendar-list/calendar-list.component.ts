import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { CalendarService } from '../calendar.service';
import { Calendar } from '../calendar.model';
import SignaturePad from 'signature_pad';


@Component({
  selector: 'app-calendar-list',
  templateUrl: './calendar-list.component.html',
  styleUrls: ['./calendar-list.component.sass']
})
export class CalendarListComponent implements OnInit {

  calendars: Calendar[] = [];

  signaturePad?: SignaturePad;
  @ViewChild('canvas') canvasEl?: ElementRef;
  signatureImg: string = "";
  courseName: string = "Cours de test";
  displayModal: boolean = false;
  displaySignature: boolean = false;

  constructor(private calendarService: CalendarService) { }

  ngOnInit(): void {
    const calendatObservable = this.calendarService.getCalendars();
    calendatObservable.subscribe((calendarsData: Calendar[]) => {
      this.calendars = calendarsData;
    });
  }

  ngAfterViewInit() {
    console.log(this.canvasEl)
    this.signaturePad = new SignaturePad(this.canvasEl?.nativeElement);
  }

  showCourseModal() {
    this.displayModal = true;
  }
  showSignature() {
    this.displaySignature = true;
  }
  startDrawing(event: Event) {
    console.log(event);
  }
  moved(event: Event) {
    // works in device not in browser
  }
  clearPad() {
    this.signaturePad?.clear();
  }
  savePad() {
    const base64Data = this.signaturePad?.toDataURL();
    this.signatureImg = base64Data ?? "";
    this.displaySignature = false;
  }

}
