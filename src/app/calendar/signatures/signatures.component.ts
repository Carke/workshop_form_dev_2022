import { Component, OnInit } from '@angular/core';
import {CalendarListComponent} from "../calendar-list/calendar-list.component";

@Component({
  selector: 'app-signatures',
  templateUrl: './signatures.component.html',
  styleUrls: ['./signatures.component.sass']
})
export class SignaturesComponent implements OnInit {

  students: Student[] = []

  constructor(private calendarList: CalendarListComponent) { }

  ngOnInit(): void {
    const students = [
      {
        id: 1,
        firstname: 'John',
        lastname: 'Doe',
        status: 'Non envoyé',
        missing: false
      },
      {
        id: 2,
        firstname: 'Philippe',
        lastname: 'Lin',
        status: 'Non envoyé',
        missing: false
      },
      {
        id: 3,
        firstname: 'Arthur',
        lastname: 'Desbois',
        status: 'Non envoyé',
        missing: false
      },
      {
        id: 4,
        firstname: 'Michel',
        lastname: 'Jonas',
        status: 'Non envoyé',
        missing: false
      }
    ]

    this.students = students
  }

  openSign(student: Student) {
    this.calendarList.showSignature();
    console.log(`openSign ${student.firstname} ${student.lastname}`)
  }

  missing(student: Student) {
    console.log(`missing ${student.firstname} ${student.lastname}`)
    this.students.forEach(s => {
      if (s.id === student.id) {
        s.missing = true
      }
    })
    console.log(student.missing)
  }
}

export interface Student {
  id: number
  firstname: String
  lastname: String
  status: String
  missing : Boolean
}
