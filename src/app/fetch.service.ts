import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FetchService {
  constructor(private http:HttpClient) { }

  getData() {
    return this.http.get("http://localhost:3000/");
  }

  tryLogin(mail:string, password:string) {
    return this.http.post("http://localhost:3000/login", {"mail": mail, "password": password});
  }
}
