const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

const corsOption = {
  origin: ['http://localhost:4200'],
};


app.use(cors(corsOption));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

let comments_1 = {
    "id": 1,
    "content": "Ronfle trop fort !",
    "datetime": "1486029264000",
    "course": "POO",
    "student": "Alan TURAING",
    "edit_time": "1486029264000"
}
let comments_2 = {
    "id": 2,
    "content": "Participe beaucoup.",
    "datetime": "1483347264000",
    "course": "Machine learning",
    "student": "Bob SPONGE",
    "edit_time": "1483350864000"
}
let comments_3 = {
    "id": 3,
    "content": "Quelques lacunes.",
    "datetime": "1483347264000",
    "course": "Photoshop",
    "student": "Joe LAGRANGE",
    "edit_time": "1483347264000"
}

let sign_1 = {
    "id": 1,
    "img": "",
    "person": "Joe LAGRANGE",
    "datetime_sign": "1483347264000",
    "datetime_send": null,
    "course": "Photoshop",
    "state": "SIGNE"
}

let sign_2 = {
    "id": 2,
    "img": "",
    "person": "Nathalie HERMANN",
    "datetime_sign": "1483347264000",
    "datetime_send": null,
    "course": "Photoshop",
    "state": "SIGNE"
}
let sign_3 = {
    "id": 3,
    "img": "",
    "person": "Cloé N'GORAN",
    "datetime_sign": null,
    "datetime_send": null,
    "course": "Photoshop",
    "state": "ABSENT"
}
let sign_4 = {
    "id": 4,
    "img": "",
    "person": "Nadia NEVERS",
    "datetime_sign": "1483347264000",
    "datetime_send": null,
    "course": "POO",
    "state": "SIGNE"
}
let sign_5 = {
    "id": 5,
    "img": "",
    "person": "Alan TURAING",
    "datetime_sign": null,
    "datetime_send": "1483347264000",
    "course": "POO",
    "state": "ABSENT"
}
let sign_6 = {
    "id": 6,
    "img": "",
    "person": "Ginette DUGUDU",
    "datetime_sign": "",
    "datetime_send": "1483347264000",
    "course": "POO",
    "state": "ATTENDU"
}
let sign_7 = {
    "id": 7,
    "img": "",
    "person": "Marguerite JUSTE",
    "datetime_sign": "1483347264000",
    "datetime_send": "1483347264000",
    "course": "POO",
    "state": "SIGNE"
}
let sign_8 = {
    "id": 8,
    "img": "",
    "person": "Frank VANDORT",
    "datetime_sign": "1483354824000",
    "datetime_send": null,
    "course": "Reseau",
    "state": "SIGNE"
}
let sign_9 = {
    "id": 9,
    "img": "",
    "person": "Antoine GELDWIN",
    "datetime_sign": "1483354824000",
    "datetime_send": null,
    "course": "Reseau",
    "state": "SIGNE"
}
let sign_10 = {
    "id": 10,
    "img": "",
    "person": "Isabelle POIRIER",
    "datetime_sign": "1483354824000",
    "datetime_send": null,
    "course": "Reseau",
    "state": "SIGNE"
}

let person_1 = {
    "id": 1,
    "firstname": "Alan",
    "lastname": "TURAING",
    "Role": "student",
    "mail": "alan.turaing@ecole.fr",
    "password": "test",
    "promotion": "B3dev",
    "options": ["IA", "Cybersec"]
}
let person_2 = {
    "id": 2,
    "firstname": "Helen",
    "lastname": "LE GALL",
    "Role": "student",
    "mail": "helen.legall@ecole.fr",
    "password": "test",
    "promotion": "B1dev",
    "options": null
}
let person_3 = {
    "id": 3,
    "firstname": "Ginette",
    "lastname": "DUGUDU",
    "Role": "student",
    "mail": "ginette.dugudu@ecole.fr",
    "password": "test",
    "promotion": "B3dev",
    "options": ["IA", "Cybersec"]
}
let person_4 = {
    "id": 4,
    "firstname": "Herbert",
    "lastname": "KLEIN",
    "Role": "student",
    "mail": "herbert.klein@ecole.fr",
    "password": "test",
    "promotion": "B2dev",
    "options": ["Cybersec"]
}
let person_5 = {
    "id": 5,
    "firstname": "Philippe",
    "lastname": "LEE",
    "Role": "student",
    "mail": "philippe.lee@ecole.fr",
    "password": "test",
    "promotion": "B2dev",
    "options": ["IA"]
}
let person_6 = {
    "id": 6,
    "firstname": "Bob",
    "lastname": "SPONGE",
    "Role": "student",
    "mail": "bob.sponge@ecole.fr",
    "password": "test",
    "promotion": "B2dev",
    "options": ["IA"]
}
let person_7 = {
    "id": 7,
    "firstname": "Cunegonde",
    "lastname": "ZADIG",
    "Role": "student",
    "mail": "cunegonde.zadig@ecole.fr",
    "password": "test",
    "promotion": "B2dev",
    "options": ["Cybersec"]
}
let person_8 = {
    "id": 8,
    "firstname": "Marguerite",
    "lastname": "JUSTE",
    "Role": "student",
    "mail": "marguerite.juste@ecole.fr",
    "password": "test",
    "promotion": "B3dev",
    "options": ["IA", "Cybersec"]
}
let person_9 = {
    "id": 9,
    "firstname": "Maximilien",
    "lastname": "GNIARUT",
    "Role": "student",
    "mail": "maximilien.gniarut@ecole.fr",
    "password": "test",
    "promotion": "B1dev",
    "options": null
}
let person_10 = {
    "id": 10,
    "firstname": "Rose",
    "lastname": "MOUTARDE",
    "Role": "student",
    "mail": "rose.moutarde@ecole.fr",
    "password": "test",
    "promotion": "B1dev",
    "options": null
}
let person_11 = {
    "id": 11,
    "firstname": "Marcel",
    "lastname": "OTTO",
    "Role": "student",
    "mail": "marcel.otto@ecole.fr",
    "password": "test",
    "promotion": "B2dev",
    "options": ["Cybersec"]
}
let person_12 = {
    "id": 12,
    "firstname": "Bertrand",
    "lastname": "VANDOOERT",
    "Role": "student",
    "mail": "bertrand.vandooert@ecole.fr",
    "password": "test",
    "promotion": "B2dev",
    "options": ["IA"]
}
let person_13 = {
    "id": 13,
    "firstname": "Joe",
    "lastname": "LAGRANGE",
    "Role": "student",
    "mail": "joe.lagrange@ecole.fr",
    "password": "test",
    "promotion": "B1Design",
    "options": null
}
let person_14 = {
    "id": 14,
    "firstname": "Cloé",
    "lastname": "N'GORAN",
    "Role": "student",
    "mail": "cloe.ngoran@ecole.fr",
    "password": "test",
    "promotion": "B1Design",
    "options": null
}
let person_15 = {
    "id": 15,
    "firstname": "Alice",
    "lastname": "NEWTON",
    "Role": "student",
    "mail": "alice.newton@ecole.fr",
    "password": "test",
    "promotion": "Mode",
    "options": null
}
let person_16 = {
    "id": 16,
    "firstname": "Henrietta",
    "lastname": "WONG",
    "Role": "student",
    "mail": "henrietta.wong@ecole.fr",
    "password": "test",
    "promotion": "Mode",
    "options": null
}
let person_17 = {
    "id": 17,
    "firstname": "Isabelle",
    "lastname": "POIRIER",
    "Role": "student",
    "mail": "isabelle.poirier@ecole.fr",
    "password": "test",
    "promotion": "B1Ops",
    "options": ["Admin"]
}
let person_18 = {
    "id": 18,
    "firstname": "Antoine",
    "lastname": "GELDWIN",
    "Role": "student",
    "mail": "antoine.geldwin@ecole.fr",
    "password": "test",
    "promotion": "B1Ops",
    "options": ["Reseau"]
}

let person_19 = {
    "id": 19,
    "firstname": "Alphonse",
    "lastname": "MARTIN",
    "Role": "teacher",
    "mail": "alphonse.martin@pro.com",
    "password": "test",
    "promotion": null,
    "options": null
}

let person_20 = {
    "id": 20,
    "firstname": "Henri",
    "lastname": "MARTINEZ",
    "Role": "teacher",
    "mail": "henri.martinez@pro.com",
    "password": "test",
    "promotion": null,
    "options": null
}

let person_21 = {
    "id": 21,
    "firstname": "Nadia",
    "lastname": "NEVERS",
    "Role": "teacher",
    "mail": "nadia.nevers@pro.com",
    "password": "test",
    "promotion": null,
    "options": null
}

let person_22 = {
    "id": 22,
    "firstname": "Nathalie",
    "lastname": "HERMANN",
    "Role": "teacher",
    "mail": "nathalie.hermann@pro.com",
    "password": "test",
    "promotion": null,
    "options": null
}

let person_23 = {
    "id": 23,
    "firstname": "Samantha",
    "lastname": "LING",
    "Role": "teacher",
    "mail": "samantha.ling@pro.com",
    "password": "test",
    "promotion": null,
    "options": null
}

let person_24 = {
    "id": 24,
    "firstname": "Frank",
    "lastname": "VANDORT",
    "Role": "teacher",
    "mail": "frank.vandort@pro.com",
    "password": "test",
    "promotion": null,
    "options": null
}

let course_1 = {
    "id": 1,
    "nom": "Algorithme",
    "date": "2017-01-01",
    "start": "10:00",
    "duration": 120,
    "teacher": "Alphonse MARTIN",
    "promotion": "B1dev",
    "options": null,
    "module": "Developpement",
    "etat": "A venir",
    "code": null,
    "students": [person_10, person_9, person_2],
    "signatures": [],
    "comments": []
}

let course_2 = {
    "id": 2,
    "nom": "POO",
    "date": "2017-02-01",
    "start": "14:00",
    "duration": 240,
    "teacher": "Henri MARTINEZ",
    "promotion": "B2dev",
    "options": null,
    "module": "Developpement",
    "etat": "En cours",
    "code": null,
    "students": [person_4, person_5, person_6, person_7, person_9, person_11],
    "signatures": [sign_4, sign_5, sign_6, sign_7],
    "comments": [comments_1]
}

let course_3 = {
    "id": 3,
    "nom": "POO",
    "date": "2017-02-02",
    "start": "08:00",
    "duration": 180,
    "teacher": "Nadia NEVERS",
    "promotion": "B3dev",
    "options": null,
    "module": "Developpement",
    "etat": "Annulé",
    "code": null,
    "students": [],
    "signatures": [],
    "comments": []
}

let course_4 = {
    "id": 4,
    "nom": "Machine learning",
    "date": "2017-01-02",
    "start": "08:00",
    "duration": 180,
    "teacher": "Nadia NEVERS",
    "promotion": "B2dev",
    "options": "IA",
    "module": "Developpement",
    "etat": "Clôturé",
    "code": null,
    "students": [person_5, person_6, person_12],
    "signatures": [],
    "comments": [comments_2]
}

let course_5 = {
    "id": 5,
    "nom": "Photoshop",
    "date": "2017-01-02",
    "start": "08:00",
    "duration": 180,
    "teacher": "Nathalie HERMANN",
    "promotion": "B1Design",
    "options": null,
    "module": "Design",
    "etat": "Clôturé",
    "code": null,
    "students": [person_13, person_14],
    "signatures": [sign_1, sign_2, sign_3],
    "comments": [comments_3]
}

let course_6 = {
    "id": 6,
    "nom": "Croquis",
    "date": "2017-01-02",
    "start": "11:00",
    "duration": 180,
    "teacher": "Samantha LING",
    "promotion": "B1Design",
    "options": null,
    "module": "Design",
    "etat": "Clôturé",
    "code": null,
    "students": [person_14, person_13],
    "signatures": [],
    "comments": []
}

let course_7 = {
    "id": 7,
    "nom": "Réseau",
    "date": "2017-01-02",
    "start": "11:00",
    "duration": 180,
    "teacher": "Frank VANDORT",
    "promotion": "B1Ops",
    "options": ["Réseau"],
    "module": "Ops",
    "etat": "Clôturé",
    "code": "AHY45",
    "students": [person_18],
    "signatures": [],
    "comments": []
}

let course_8 = {
    "id": 8,
    "nom": "Active Directory",
    "date": "2017-02-01",
    "start": "15:00",
    "duration": 120,
    "teacher": "Frank VANDORT",
    "promotion": "B1Ops",
    "options": ["Admin"],
    "module": "Ops",
    "etat": "En cours",
    "code": null,
    "students": [person_17],
    "signatures": [],
    "comments": []
}

let module_1 = {
    "id": 1,
    "nom": "Developpement",
    "courses": [course_1, course_2, course_4]
}

let module_2 = {
    "id": 2,
    "nom": "Design",
    "courses": [course_5, course_6]
}

let module_3 = {
    "id": 3,
    "nom": "Ops",
    "courses": [course_7, course_8]
}

let module_4 = {
    "id": 4,
    "nom": "Mode",
    "courses": [course_1, course_2, course_4]
}

app.get('/', (req,res) => {
    const json_data = {
        "1": module_1,
        "2": module_2,
        "3": module_3,
        "4": module_4
    }
    res.send(json_data)
})

app.post('/login', (req,res) => {
    let persons = [
        person_1, person_2, person_3, person_4, person_5, person_6,
        person_7, person_8, person_9, person_10, person_11, person_12,
        person_13, person_14, person_15, person_16, person_17, person_18,
        person_19, person_20, person_21, person_22, person_23, person_24
    ];

    persons.forEach(function(person) {
        let mail = person.mail;
        let pwd = person.password;

        if (mail == req.body["mail"]) {
            if (pwd == req.body["password"]) {
                res.send({
                    "response": "ok"
                });
            }
        }
    })
    res.send({
            "response": "ko"
        });
})

app.get('/health',(req,res) => {
  res.send('I')
})

app.listen(3000,() => {
  console.log("Server up and running")
})
